#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import date

오늘 = date.today().strftime("%Y-%m-%d")
날수 = 오늘[-1] # <class 'str'>

큰마음 = [
    "_布德天下_",
    "_白衣從軍_",
    "_救濟蒼生_",
    "_和合團結_",
]

기본값 = "_地平天成_" # 우임금의 나라와 백성 위한 마음

SIG_FPATH = "/home/soyeomul/.signature"

f = open(SIG_FPATH, "r")

text = f.read(); f.close()

if 날수 == "3" or 날수 == "8": # 삼팔
    signature = text.replace(기본값, 큰마음[0])
elif 날수 == "4" or 날수 == "9": # 사구
    signature = text.replace(기본값, 큰마음[1])
elif 날수 == "5" or 날수 == "0": # 오십
    signature = text.replace(기본값, 큰마음[2])
elif 날수 == "1" or 날수 == "6": # 일육
    signature = text.replace(기본값, 큰마음[3])
else: # 이칠
    signature = text

print(signature)

# 편집: GNU Emacs 26.3 (Ubuntu 18.04)
# 마지막 갱신: 2020년 3월 25일
