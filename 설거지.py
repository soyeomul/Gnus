#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import subprocess

# 환경: 우분투 18.04 LTS 

SECRET = "SECRET" # sudo 비밀번호를 이곳에 적습니다

do_update = "echo {} | sudo -S apt-get update".format(SECRET)
do_upgrade = "echo {} | sudo -S apt-get -y upgrade".format(SECRET)
_message = "\n 이제 다음단계로 넘어가네유 ~~~ \n\n"

def 설거지():
    subprocess.call(do_update, shell=True)
    sys.stdout.write(_message)
    sys.stdout.flush()
    time.sleep(3)
    subprocess.call(do_upgrade, shell=True)

if __name__ == "__main__":
    sys.exit(설거지())

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 5월 4일
