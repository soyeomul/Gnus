#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE, call

# 구글 클라우드 플랫폼에 설치된 Postfix 자원을 호출하기위한 사전 작업
# gcp 인터넷 주소는 /etc/hosts 에 등록되어 있음

_UID = "soyeomul" # 변경 가능합니다^^^
SSH_RSA = "gcp_rsa" # 변경 가능합니다^^^
REMOTE_ADDR = "gcp" # 변경 가능합니다^^^
LOCAL_ADDR = "localhost" # 변경 가능합니다^^^
MESSAGE_FPING = "{0} is alive".format(REMOTE_ADDR)
SMTP_BANNER = """\
220 yw-0919.doraji.xyz ESMTP Postfix (Ubuntu/Google-Compute-Engine)\
""" # 역시 변경 가능합니다^^^

def _check(xyz):
    if xyz == "fping":
        _cmd = "{0} {1}".format(xyz, REMOTE_ADDR)
    if xyz == "curl":
        _cmd = "{0} -s -f {1}:2000".format(xyz, LOCAL_ADDR)
    if xyz == "ssh":
        _cmd = "ps -ax | grep {0} | grep {1} | grep -v '/bin/sh'".format(xyz, REMOTE_ADDR)
        
    _try = Popen(_cmd, stdout=PIPE, shell=True)

    if xyz == "ssh":
        output = _try.communicate()[0].decode("utf-8").strip().split()[0]
    else:
        output = _try.communicate()[0].decode("utf-8").strip()

    return output

if MESSAGE_FPING not in _check("fping"):
    exit("인터넷 연결이 끊어졌습니다.")
else:
    print("인터넷 연결 OK!")
    try:
        ssh_pid = _check("ssh")
        call("kill -HUP {0}".format(ssh_pid), shell=True)
        print("ssh 연결 세션 새로이 갱신~")
    except:
        pass

if SMTP_BANNER not in _check("curl"):
    try:
        call("sleep 11; ssh -i ~/.ssh/{0} -f {1}@{2} -L 2000:{3}:25 -N"\
             .format(SSH_RSA, _UID, REMOTE_ADDR, LOCAL_ADDR), shell=True)
    except:
        exit("알 수 없는 에러가 발생했어요!")

print("포트 2000 무사히 열었습니다^^^", "===>", "PID: {0}".format(_check("ssh")))

# 참고문헌: https://www.rackaid.com/blog/spam-ssh-tunnel/

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2022년 4월 2일
