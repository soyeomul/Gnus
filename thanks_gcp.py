#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json

#
# 시간 기반 랜덤 함수
#
def trandom(length):
    from time import time
    """
    random 모듈이 좀 무겁다는 생각에...
    저녁 소여물 주면서 계속 랜덤함수를 생각했어요
    끊임없이 변하는것을 변하지 않는것으로 나누면 좋겠다싶었어요
    변하는건 시간, 변하지않는건 공간 그래서 이 함수가 만들어졌어요

    부처님오신날이라 그런지
     이게 꼭 불교의 윤회(자연의 순환-반복하는 이치)처럼 느껴졌습니다
    --소여물, 2020년 4월 30일 (음력 4월 8일)
    """
    _constant = 108 * 361 * 13 # 시간을 좀 더 세분화시킴
    vtime = int(time() * _constant)

    return vtime % length


#
# 파일 내려받기 함수
#
def _get_url(xyz):
    from subprocess import Popen, PIPE
    """
    `curl' 은 Linux/*BSD 등에서 유명한 도구입니다
    본 코드는 그래서 가급적 Linux/*BSD 시스템에서 실험하시길 권유드립니다
    """
    _cmd = "curl -s -f {0}".format(xyz)
    _try = Popen(_cmd, stdout=PIPE, shell=True)

    output = _try.communicate()[0].decode("utf-8").strip()

    return output # 문자열(str)


#
# hex ===> str
#
# 간단해보이지만 필요할때에 짜내려면
#  관련 문서를 찾아봐야할 정도로 가벼이 볼 수 없는 함수 
#
def hts(xyz):
    n = int(xyz, 16)

    return chr(n), xyz


def _(xyz):
    _CI_PROJECT_URL = "https://gitlab.com/soyeomul/Gnus"
    _CI_COMMIT_BRANCH = "MaGnus"
    _FILE_NAME = xyz

    url = "%s/-/raw/%s/%s/%s" % (
        _CI_PROJECT_URL,
        _CI_COMMIT_BRANCH,
        "말씀",
        _FILE_NAME,
    )

    return url


# 제한범위를 넘길경우 교법2장19절 을 자동 반환함 
기본값 = _get_url(_("교법2장19절.txt"))
_mask = 256 # 0x0000 - 0x00ff
_뭉치 = "말씀.json"
_high_voltage = "0x26A1" # U+26A1
fdata = _get_url(_(_뭉치)) # <class 'str'>


def _output(_data):
    dj = json.loads(_data)    
    length = len(dj.keys())
    idx = trandom(length)

    return dj['{0}'.format(idx)]


output = _output(fdata)[1:-1]
if len(output) > _mask:
    output = 기본값

    
__thanks__ = "%s %s %s" % (
    hts(_high_voltage)[0] * 3,
    output,
    hts(_high_voltage)[0] * 3,
)


def _today():
    return __thanks__


#
# Thanks!!!
#

if __name__ == "__main__":
    print(_today())


# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2023년 4월 9일
