;;; ~/.emacs
;;; -*- coding: utf-8 -*-

;;; 모든 문자를 항상 UTF-8 로 보냅니다~ 
;; 요즘 시대는 UTF-8 이 대세랍니다..!!!
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(setq system-time-locale "C.UTF-8")

;;; 한글이 너무 좋아요!
;; ### 글꼴 (UTF-8)
;; 구글(Google)의 개방형 글꼴 "Noto Sans CJK"
;; 저작권 유형: OFL
;; UTF-8 환경을 가장 완벽하게 받쳐주는 개방형 글꼴로써,
;; Chrome OS, Android 등의 구글 제품에 주력으로 쓰입니다.
;; 또한 옛한글도 아주 잘 보여줍니다^^
;; URL: https://github.com/googlei18n/noto-cjk
(set-fontset-font "fontset-default"
		  'hangul "Noto Sans CJK KR")

;;
;; ### UTF-8 환경에서 한글을 입력해봅니다^^
;; 아래 설정으로 이맥스에서 한글을 입력할 수 있으며,
;; 이맥스 한글 입력기를 만들어주신 조지현님께 감사드리며^^
;; URL: https://www.emacswiki.org/emacs/hangul.el
(set-input-method "korean-hangul")
;;
;; ### 한/영 전환으로 ["CTRL"+"SPACE"] 조합을 쓰려합니다.
;; 기본값: ["CTRL"+"\"]
(global-set-key (kbd "<C-SPC>") 'toggle-input-method)
;(global-set-key [f9] 'hangul-to-hanja-conversion)

;; ### Input Methods 추가 사항
;; ! 이맥스 내장 입력기를 쓰기위하여,
;; ! 추가적으로 ~/.Xresources 파일에
;; ! XIM 입력기를 끄는 설정을 넣으셔야 합니다.
;; ! URL: https://www.emacswiki.org/emacs/InputMethods
;; 추가: Wayland 시대에서는 본 설정은 무의미 -- 2021-12-13

;;; 뉴스 및 전자메일에 Gnus 를 주력으로 씁니다.
(setq mail-user-agent 'gnus-user-agent)
(setq send-mail-function 'sendmail-send-it)

;;; 인터넷 대화방 참여하기 (IRC)
;; irc.libera.chat (우분투 공식)
(setq user-login-name "soyeomul")
(setq erc-email-userid "soyeomul")
(setq erc-prompt-for-password nil)
(setq erc-user-full-name "황병희")
(setq erc-server "irc.libera.chat")
;;; channel: #ubuntu-kr

;;; C언어 공부, 왜냐믄 토발즈행님을 무쟈게 좋아하는 까닭에...;;;
(setq c-default-style "linux")

;;;;; 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
;;;;; 마지막 갱신: 2024년 1월 24일
