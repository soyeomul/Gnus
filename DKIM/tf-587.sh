#!/bin/bash
# -*- coding: utf-8 -*-

# File: /etc/init.d/tf-587.sh
# HOSTNAME: yw-1204.doraji.xyz
# Description:	Debian GNU/Linux 11 (bullseye)

### BEGIN INIT INFO
# Required-Start:    $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
### END INIT INFO

set -x

SR="0.0.0.0/0"

case "$1" in
    start)
	sudo iptables -v -t nat -A PREROUTING -s $SR -p tcp --dport 587 -j REDIRECT --to-port 22
	;;
    status)
	sudo iptables -t nat -L --line-numbers
	;;
    stop)
	sudo iptables -v -t nat -D PREROUTING 1
	;;
    *)
	echo "Usage: /etc/init.d/tf-587.sh {start|status|stop}"
	exit 1
	;;
esac
      


# 배경:
# submission(587) 포트로 접근해오면 22 포트로 방향을 돌립니다 -- 멍텅구리 작전
# IPv6 클라이언트만 접근을 허용합니다 

# 참고문헌: 
# [1] https://velog.io/@shawnhansh/Ubuntu-%ED%8F%AC%ED%8A%B8%ED%8F%AC%EC%9B%8C%EB%94%A9Portforwarding
# [2] https://klkl0.tistory.com/127
# [3] https://serverfault.com/questions/981433/script-set-up-via-update-rc-d-not-being-called-on-startup
# [4] <<처음 배우는 셸 스크립트 (장현정) -- ISBN 9791162243893>>

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막갱신: 2024-01-28
