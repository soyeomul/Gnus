#!/bin/bash
# -*- coding: utf-8 -*-

# File: /etc/init.d/tf.sh
# HOSTNAME: yw-0919.doraji.xyz
# Description:	Ubuntu 18.04.6 LTS

### BEGIN INIT INFO
# Required-Start:    $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
### END INIT INFO

set -x

VKO="`host -t aaaa out1.vger.email. | awk '{print $5}'`"
CYT="`host -t a salsa.cs.uic.edu. | awk '{print $4}'`"
MLO="212.83.56.199"

case "$1" in
    start)
	sudo ip6tables -v -t nat -A PREROUTING -s $VKO -p tcp --dport 25 -j REDIRECT --to-port 2525
	sudo iptables -v -t nat -A PREROUTING -s $CYT -p tcp --dport 25 -j REDIRECT --to-port 2525
	sudo iptables -v -t nat -A PREROUTING -s $MLO -p tcp --dport 25 -j REDIRECT --to-port 2525
	;;
    stop)
	sudo ip6tables -v -t nat -D PREROUTING 1
	sudo iptables -v -t nat -D PREROUTING 1
	sudo iptables -v -t nat -D PREROUTING 1
	;;
    *)
	echo "Usage: /etc/init.d/tf.sh {start|stop}"
	exit 1
	;;
esac
      


# 배경:
# 다음 두 메일서버 <<out1.vger.email>> <<salsa.cs.uic.edu>> 들은 보안연결을 지원하지 않습니다
# (mail.lindenberg.one 추가 -- 2023-05-19)
# 상기 두 서버가 yw-0919 25번 포트로 접근해올때 2525 포트로 진행방향을 돌립니다
# 구글 클라우드 방화벽 규칙을 새로이 만들되 TCP 2525 포트 ipv6/ipv4 수신을 받아들이는 규칙을 만듭니다
# 이렇게 하므로써 도라지 메일서버에서 smtpd_tls_security_level=encrypt 정책을 유지할 수 있습니다
# (... 도라지 메일서버는 MTA-STS (RFC 8461) 실험중 ...)
# 본 스크립트는 구글 클라우드 VM 우분투 18.04 LTS 에서 실험했으며 운용중입니다 

# 구글 클라우드 방화벽 상황:
# default-allow-smtp6-vger: IPv6 TCP 2525 INGRESS;
# ===> ::/0 (IPv6 는 세상 모든곳에서의 접근을 허용합니다)
# default-allow-smtp-vger: IPv4 TCP 2525 INGRESS;
# ===> 10.128.0.0/20 (구글 클라우드 내부망)
# ===> 35.239.123.232/32 (yw-0919.doraji.xyz)
# ===> 185.17.255.72/32 (yw-1204.doraji.xyz)
# ===> 23.128.96.0/24 (vger.kernel.org -- ASN: AS53758 INVALID-NETWORK, US)
# ===> 131.193.0.0/16 (cr.yp.to -- ASN: AS6200 UIC-AS, US)
# ===> 212.83.56.0/24 (lindenberg.one -- ASN: AS47447 TTM, DE)

# 참고문헌: 
# [1] https://velog.io/@shawnhansh/Ubuntu-%ED%8F%AC%ED%8A%B8%ED%8F%AC%EC%9B%8C%EB%94%A9Portforwarding
# [2] https://klkl0.tistory.com/127
# [3] https://serverfault.com/questions/981433/script-set-up-via-update-rc-d-not-being-called-on-startup
# [4] <<처음 배우는 셸 스크립트 (장현정) -- ISBN 9791162243893>>

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막갱신: 2023-10-12
