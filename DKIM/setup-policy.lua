-- -*- coding: utf-8 -*-
-- /home/soyeomul/git/karma/Gnus/DKIM/setup-policy.lua

---
--- Dummy Verification
---

fdomain = odkim.get_fromdomain(ctx)
if fomain ~= "doraji.xyz" then
        odkim.verify(ctx)
end

---
--- soyeomul@doraji.xyz 
---

-- RFC2822.From ===> get_header()
from2822 = odkim.get_header(ctx, "From", 0)
odkim.log(ctx, "RFC2822.From:"..from2822)

-- RFC2822.Reply-To ===> get_header()
rpto2822 = odkim.get_header(ctx, "Reply-To", 0)
if rpto2822 ~= nil then
	odkim.log(ctx, "RFC2822.Reply-To:"..rpto2822)
end

-- RFC2822.User-Agent ===> get_header()
ua2822 = odkim.get_header(ctx, "User-Agent", 0)
if ua2822 ~= nil then
	odkim.log(ctx, "RFC2822.User-Agent:"..ua2822)
end

-- RFC2822.X-Mailer ===> get_header()
xm2822 = odkim.get_header(ctx, "X-Mailer", 0)
if xm2822 ~= nil then
	odkim.log(ctx, "RFC2822.X-Mailer:"..xm2822)
end

-- RFC2821.MailFrom ===> get_envfrom()
from2821 = odkim.get_envfrom(ctx)
odkim.log(ctx, "RFC2821.MailFrom: "..from2821)

-- RFC4954.Auth ===> get_mtasymbol()
auth4954 = odkim.get_mtasymbol(ctx, "{auth_authen}")
if auth4954 ~= nil then
	odkim.log(ctx, "RFC4954.Auth: "..auth4954)
end

--[[
Hwa-Pyeong-Ui-Gil 는 keytable 에 존재해야하며
SignatureAlgorithm 에 지정된 방식으로 서명을 합니다.

SignatureAlgorithm 의 기본값은 RSA-SHA256  이며
ED25519-SHA256 을 선택적으로 도입할 수도 있습니다.

아래의 키(Hwa-Pyeong-Ui-Gil (4096 bits))는 RSA-SHA256 으로 서명합니다.
]]--
local AUID = "soyeomul@doraji.xyz"
local AUTH = "soyeomul@YW"
if string.find(from2822, AUID, 1, true) ~= nil and from2821 == AUID and auth4954 == AUTH then 
	odkim.use_ltag(ctx)
	odkim.sign(ctx, "Hwa-Pyeong-Ui-Gil", "soyeomul@doraji.xyz")
end

---
--- forwarding to Gmail
---

-- RFC2821.ORCPT ===> get_rcpt()
to2821 = odkim.get_rcpt(ctx, 0)
odkim.log(ctx, "RFC2821.ORCPT: "..to2821)

--[[
구글 Gmail 로 포워딩 합니다. 구글 Gmail 서버는
DKIM 서명 검증 통과한 메일들만 거부하지 않고 받아들입니다.

구글 Gmail 정책에 따르면 DKIM 서명키는 일종의 출발지 통행증(Passport)의 역할을 합니다.
이 통행증인 DKIM-Signature 를 지참하고 구글 Gmail 서버에 도착한 메일들만이
거부되지 않고 받아들여집니다.
(...당연 검증도 통과해야 하지요...:  "dkim=pass")

또한 구글은
DKIM 서명키를 또다른 평판(SMTP Server's Reputation) 요소로 다룹니다.

아래 RCPT 테이블은 포워딩 될 최종 목적지 Gmail 주소들입니다.
]]--
local RCPT = {
        "soyeomul+gcp@gmail.com",
        "soyeomul+ed25519@gmail.com",
}

-- ...포워드 전용 아웃본드(Outbond SMTP Server) 키를 서명 합니다...
-- ...통행증(Passport)을 만드는 작업입니다...
-- (도라지 메일서버는 YW키 이외에 아웃본드 기본키가 별도로 따로 있습니다)
-- (도라지 아웃본드 기본키 ===> https://doraji.xyz/)
if to2821 == RCPT[1] or to2821 == RCPT[2] then
        odkim.sign(ctx, "YW")
end

---
--- ^THANKS_^))//
---

-- ^고맙습니다 _布德天下_ 감사합니다_^))//
local file = io.open("/home/soyeomul/git/karma/Gnus/DKIM/thanks.YW")
local THANKS = {}
local i = 0
if file then
        for line in file:lines() do
                i = i + 1
                THANKS[i] = line
        end
        file:close()
end

relay_hostname = odkim.get_clienthost(ctx)
local THANKS_HOSTNAME = "yw-0919.doraji.xyz"

function compute(abc, xyz)
	for key,value in pairs(abc) do
		if value == xyz then
			return true
		end
	end
end

if compute(THANKS, to2821) ~= nil and relay_hostname == THANKS_HOSTNAME then
	odkim.sign(ctx, "Yeon-San-Hong")
end

---
--- TEST
----

-- get rcvd header
rcvd2822 = odkim.get_header(ctx, "Received", -1)
if rcvd2822 ~= nil then
	odkim.log(ctx, "RFC2822.RCVD-1:"..rcvd2822)
end

-- get the From: domain
--fdomain = odkim.get_fromdomain(ctx)
if fdomain ~= nil then
        odkim.log(ctx, "RFC5598.ADMD (Best Guess): "..fdomain)
end


-- 참고문헌: [0-3] 
-- 0. <https://www.m3aawg.org/system/files/DKIM_Segment4_Practical-B_0.pdf>
-- 1. <https://gitlab.com/soyeomul/Gnus/-/raw/master/DKIM/GMAIL-POLICY>
-- 2. opendkim-lua(3) `man 3 opendkim-lua`
-- 3. [RFC5598] D. Crocker, "Internet Mail Architecture"

-- 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
-- 마지막갱신: 2024-02-20
