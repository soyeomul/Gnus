-- -*- coding: utf-8 -*-
-- /home/soyeomul/git/karma/Gnus/DKIM/final-policy.lua

-- nested signature deleting from soyeomul@doraji.xyz
local TD = "doraji.xyz"
local RH = "DKIM-Signature"
rcpt = odkim.get_rcpt(ctx, 0)
fdomain = odkim.get_fromdomain(ctx)
rstr = odkim.get_header(ctx, "Reply-To", 0)

if string.find(rcpt, "soyeomul+gcp@gmail.com", 1, true) ~= nil and fdomain == TD then
        odkim.del_header(ctx, RH, -1)
        odkim.del_header(ctx, RH, -1)
end

if rstr ~= nil then
	if string.find(rcpt, "soyeomul+gcp@gmail.com", 1, true) ~= nil and string.find(rstr, TD, 1, true) ~= nil then
		odkim.del_header(ctx, RH, -1)
		odkim.del_header(ctx, RH, -1)
	end
end


-- odkim.add_header(ctx, name, value)
-- local thanks_header = "X-YW-DKIM-Comments"
-- local thanks_header_val = "See http://doraji.xyz/ YW Towers ^^^ Thanks! Thanks! Thanks!;"
-- odkim.add_header(ctx, thanks_header, thanks_header_val)
-- odkim.log(ctx, "added new header: "..thanks_header)


-- 참고문헌: [1-3]
-- [1] https://www.m3aawg.org/system/files/MAAWG_DKIM_Complete_1-4a_1.pdf
-- [2] https://kyudy.hatenablog.com/entry/2013/09/14/041641
-- [3] opendkim-lua(3) `man 3 opendkim-lua`

-- 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
-- 마지막갱신: 2024-01-28
