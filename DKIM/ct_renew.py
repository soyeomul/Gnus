#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Date: Fri, 29 Apr 2022 16:39:49 +0000 (UTC)
From: "Elton M. Labajo" <elton@rimuhosting.com>
Reply-To: support@rimuhosting.com
To: Byung-Hee HWANG <soyeomul@doraji.xyz>
Cc: soyeomul@gmail.com
Message-ID: <29168327.63.1651250389220@localhost>
In-Reply-To: <xhmtuac58l3.fsf@yw-1130>
References: <xhmilqsjgjt.fsf@yw-1130> <2456327.57.1651234469218@localhost> <xhmtuac58l3.fsf@yw-1130>
Subject: Re: Let's Encrypt auto renew
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: quoted-printable
User-Agent: COMMAPP

Hi,
=20
   Usually letsencrypt cronjob has the following entry to auto renew the SS=
L certs.

# Add this to the crontab and save it:
* 7,19 * * * certbot -q renew

Regards,
Elton
Rimuhosting Support
http://ri.mu - Startups start here.  VPS and Dedicated Hosting.  DNS. Web P=
rogramming.  Email.  Backups.  Monitoring.
"""

import subprocess
import time
import sys

cmd_cert = "sudo certbot renew"
cmd_postfix = "sudo postfix reload"

def renew():
    subprocess.call(cmd_cert, shell=True)
    time.sleep(3)
    subprocess.call(cmd_postfix, shell=True)


if __name__ == "__main__":
    sys.exit(renew())

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2022년 4월 30일
