#!/bin/sh
# -*- coding: utf-8 -*-

# making ed25519 key for DKIM
# https://www.mailhardener.com/kb/how-to-use-dkim-with-ed25519
# tested version of opendkim: 2.11.0 (Debian 11 Bullseye) 

### try as root user ###

SN="ed25519" # selector name -- change this one
OWNER="opendkim" # owner of opendkim
GROUP="opendkim" # group of opendkim
DIR="/etc/dkimkeys" # location of keys

openssl genpkey -algorithm ed25519 \
-out $SN.private \
&& \
openssl pkey -in $SN.private \
-pubout -out $SN.pub \
&& \
openssl asn1parse -in $SN.pub \
--offset 12 -noout -out /dev/stdout | openssl base64 > $SN.txt

chown -v $OWNER.$GROUP $SN.private $SN.txt
rm -vf $SN.pub

echo "===> then copy both files ($SN.private and $SN.txt) to $DIR"
echo "===> $SN.txt is for DNS setup"

# Description:	Debian GNU/Linux 11 (bullseye)
# 2023-04-29
