#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE, call
import os

###
### Postfix/OpenDKIM 그리고 /etc/aliases, /etc/resolv.conf 영역
###

def alias_diff():
    cmd = "sudo cat /etc/aliases | grep -v '#' | sed '/^$/d'"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )

    return rrr


def resolv_conf():
    cmd = "sudo cat /etc/resolv.conf | grep -v '#' | sed '/^$/d'"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )

    return rrr


def main_diff():
    cmd = "sudo postconf -n"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )
    
    return rrr


def master_diff():
    cmd = "sudo postconf -Mf"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )

    return rrr


def transport():
    cmd = "sudo cat /etc/postfix/transport | grep -v '#' | sed '/^$/d'"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )

    return rrr


def tls_policy():
    cmd = "sudo cat /etc/postfix/tls_policy | grep -v '#' | sed '/^$/d'"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )

    return rrr


def header_checks():
    cmd = "sudo cat /etc/postfix/header_checks.regexp | grep -v '#' | sed '/^$/d'"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )

    return rrr


def body_checks():
    cmd = "sudo cat /etc/postfix/body_checks.regexp | grep -v '#' | sed '/^$/d'"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )

    return rrr


def sender_canonical():
    cmd = "sudo cat /etc/postfix/sender_canonical | grep -v '#' | sed '/^$/d'"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )

    return rrr


def sender_relay():
    cmd = "sudo cat /etc/postfix/sender_relay"
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        "===> See `crontab -l'. Do comment the line if you have problem.",
    )

    return rrr


def sasl_diff():
    cmd = "sudo cat /etc/postfix/sasl/smtpd.conf"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        cmd,
        rr,
    )

    return rrr


def opendkim_conf():
    pcmd1 = "sudo cat /etc/opendkim.conf > /tmp/asdf.txt"
    pcmd2 = "sudo cat /etc/dkimkeys/trustedhosts >> /tmp/asdf.txt"
    call(pcmd1 + ";" + pcmd2, shell=True)
    cmd = "sudo cat /tmp/asdf.txt | grep -v '#' | sed '/^$/d'"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s" % (
        "#",
        "/etc/opendkim.conf (including truestedhosts)",
        rr,
    )

    return rrr


###
### 사용자 영역 
###

def dot_forward():
    cmd = "sudo cat ~soyeomul/.forward | grep -v '#' | sed '/^$/d'"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s\n%s\n%s" % (
        "#",
        cmd,
        rr,
        "# ===> and please remove ~/.forward file if you have serious problem such as *loop*." 
    )

    return rrr




###
### 시스템 영역
###

def cdate():
    cmd = "date --utc"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s" % ("#", rr)
    
    return rrr


def lsb_release():
    cmd = "lsb_release -d"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s" % ("#", rr)

    return rrr


def mail_version():
    cmd = "sudo postconf mail_version"
    r = Popen(cmd, stdout=PIPE, shell=True)
    rr = r.communicate()[0].decode("utf-8").strip()
    rrr = "%s %s %s" % ("#", rr, "(Postfix)")

    return rrr


###
### Thanks! Thanks! Thanks! 
###

if __name__ == "__main__":
    print("# -*- coding: utf-8 -*-")
    print(alias_diff())
    print(resolv_conf())
    print(main_diff())
    if os.path.isfile('/etc/postfix/sender_canonical'):
        print(sender_canonical())
    if os.path.isfile('/etc/postfix/sender_relay'):
        print(sender_relay())
    if os.path.isfile('/etc/postfix/transport'):
        print(transport())
    if os.path.isfile('/etc/postfix/tls_policy'):
        print(tls_policy())
    if os.path.isfile('/etc/postfix/header_checks.regexp'):
        print(header_checks())
    if os.path.isfile('/etc/postfix/body_checks.regexp'):
        print(body_checks())
    if os.path.isfile('/etc/postfix/sasl/smtpd.conf'):
        print(sasl_diff())
    print(master_diff())
    if os.path.isfile('/etc/opendkim.conf'):
        print(opendkim_conf())
    if os.path.expanduser("~soyeomul") == "/home/soyeomul":
        print(dot_forward())
    print(cdate())
    print(lsb_release())
    print(mail_version())

    
# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2024년 1월 21일
