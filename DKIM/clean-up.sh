#!/bin/bash
# -*- coding: utf-8 -*-

# REFERENCE:
# https://lists.debian.org/debian-user/2023/04/msg01201.html

KARMA="/home/soyeomul/karma/Mailbox.karma"
KARMA_HTML="/home/soyeomul/karma/Mailbox.html"
KARMA_GIT="/home/soyeomul/karma/Mailbox.gitster"

sudo sh -c "cat /dev/null | dd of=$KARMA"
sudo sh -c "cat /dev/null | dd of=$KARMA_HTML"
sudo sh -c "cat /dev/null | dd of=$KARMA_GIT"
rm -f /home/soyeomul/Mailbox

# This script works every Tuesday
# 0 7  * * 2 /home/soyeomul/git/karma/Gnus/DKIM/clean-up.sh
# every month first day 
# 0 0  1 * * /home/soyeomul/git/karma/Gnus/DKIM/clean-up.sh

# EDITOR: GNU Emacs 27.1 (Debian 11 Bullseye)
# Last Updated: 2024-01-17
