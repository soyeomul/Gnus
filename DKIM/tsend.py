#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from datetime import datetime

today = datetime.utcnow().strftime("%Y-%m-%d")
cmd = "/home/soyeomul/bin/thanks_gcp.py"
muttrc = "/home/soyeomul/.muttrc-tosagong"
base64string = "dGhhbmtz" # some uid based on /etc/aliases

def lp(xyz):
    """
    BASE64 로 인코딩된 문자열을 원래 문자열로 디코딩합니다
    전자메일 무단 수집을 방어하기 위한 임시방편책입니다
    참고문헌: https://hbase.tistory.com/399
    """
    import base64

    xyz_bytes = xyz.encode("utf-8")
    xyz_decode = base64.b64decode(xyz_bytes)

    return xyz_decode.decode("utf-8")


def rcpt():
    __YW__ = "yw.doraji.xyz"
    local_uid = lp(base64string)

    return "%s@%s" % (local_uid, __YW__)


mailto = rcpt()


if __name__ == "__main__":
    subprocess.call("{0} | mutt -F {1} -s 'Thanks Today {2}' {3}".format(cmd, muttrc, today, mailto), shell=True)  

# 2023-05-29
