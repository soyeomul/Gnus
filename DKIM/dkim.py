#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE

__maintainer__ = "soyeomul@doraji.xyz (Byung-Hee HWANG)"

### DKIM (RFC 4871) STATUS: DORAJI.XYZ ###
domain = "doraji.xyz"
selector = "yw-1204-doraji-xyz"
"""
----------: historical selector 'google' (1024 bits)
----------: historical selector 'google-2048-bit' (2048 bits)
----------: historical selector 'yw' (2048 bits)
----------: historical selector '20220208.yw.google' (2048 bits)
----------: historical selector 'very-special-thanks-to-yw.google' (2048 bits)
2016-03-06: domain created at Gabia (IANA-ID: 244)
2016-03-08: droped anchor at <<Google Apps>> 
2018-01-04: domain moved to Tucows (IANA-ID: 69)
2022-02-08: begins '20220208.yw.google'
2022-02-22: added 's=email; n=Google Workspace (2048 bits);'
2022-02-27: added 't=y;'
2022-03-06: updated 't=s;'
2022-03-07: domain moved to Megazone (IANA-ID: 1489)
2022-03-07: updated ADSP 'dkim=all'; RFC 5617
2022-03-07: added maintainer into 'n=' tag
2022-03-08: updated to test mode again!
2022-03-11: upgrade to <<Google Workspace (Business Starter)>>
2022-03-12: updated 'n=' tag (References) 
2022-03-15: begins 'very-special-thanks-to-yw.google'
2022-03-16: updated Whois Record 'Registrant Organization'
2022-03-22: updated RO to 'YW Towers' -- 靑鷄塔;
2022-03-31: begins 'yw-1204-doraji-xyz' with RIMUHOSTING-EU
2022-04-03: updated notes; added REGION (Frankfurt, Germany)
2022-04-09: updated MX priority; yw-0919(1871)/yw-1204(1895)/...(1917) 
2022-04-12: opendkim.conf update as 'MultipleSignatures True'
2022-04-14: updated notes; added RFC5617 and RFC6541
2022-04-16: MX servers from now no goes with <<yw-0919>> and <<yw-1204>>
2022-05-17: Back to the Google MX with 'no-cost Legacy G Suite'
2022-05-18: Goodbye Google Workspace
"""
###

def _target(abc):
    _domain = domain
    _selector = abc

    return "%s.%s.%s." % (
        _selector,
        "_domainkey",
        _domain,
    )


def _query(xyz):
    _cmd = "host -t txt %s" % (xyz)

    _try = Popen(_cmd, stdout=PIPE, shell=True)

    output = _try.communicate()[0].decode("utf-8")

    return output


if __name__ == "__main__":
    print(_query(_target(selector)))

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2022년 5월 18일
