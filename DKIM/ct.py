#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
import sys

# <<<메일서버의 신용보증서 상태를 파악합니다>>>
# - 보증서 유효기간
# - 보증서 지문 
# - 보증서 검증
#
# 실험 검증 환경: Ubuntu 18.04 LTS / Debian 11 Bullseye
#
# REFERENCEs: 
# [0] <https://ismailyenigul.medium.com/openssl-tips-ae7c9cccbbaa>
# [1] <https://www.mail-archive.com/postfix-users@postfix.org/msg87674.html>
# [2] MTA-STS (RFC 8461) 

# Public Domain

SMTPSERVER = sys.argv[1] # Server's FQDN
PORT = "25" # We check MX servers by default
CAfile = "/etc/ssl/certs/ca-certificates.crt"

cmd_fingerprint = """\
sudo posttls-finger \
-F %s -d sha256 -c %s:%s""" % (CAfile, SMTPSERVER, PORT)

cmd_date = """\
printf 'quit\n' | openssl s_client -connect %s:%s \
-starttls smtp | openssl x509 -dates -noout""" % (SMTPSERVER, PORT)

def check():
    subprocess.call(cmd_date, shell=True)
    print("^^^")
    subprocess.call(cmd_fingerprint, shell=True) 


if __name__ == "__main__":
    sys.exit(check())

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2022년 5월 1일
