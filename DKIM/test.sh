#!/usr/bin/env bash
# -*- coding: utf-8 -*-

# 환경: Ubuntu 18.04 LTS
# 필요한 패키지를 설치합니다
#
### 참고문헌:
# <<처음 배우는 셸 스크립트 -- ISBN 9791162243893>>, 45페이지

apt-get update -qq
apt-get install -y -qq python3
apt-get install -y -qq host

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2022년 2월 22일
