# -*- coding: utf-8 -*-

# custom module(s)

"""
- 임의의 $PYTHONPATH 확인하는 방법

import sys
print(sys.path)

- 임의의 $PYTHONPATH 추가하는 방법

import sys
sys.path.insert(0, "/home/soyeomul/__system__")
 또는
sys.path.append("/home/soyeomul/__system__")
"""

#
# 절대경로 구하는 함수
#
def _full_path(xyz):
    from subprocess import Popen, PIPE, call 
    """
    '~' 부호는 파이썬 경로명에서 잘 안먹힘
    '~' 부호도 파이썬에서 먹히게끔 해줌
    """
    call("mkdir -p {0}".format(xyz), shell=True)
    p = Popen("cd {0}; pwd -P".format(xyz), stdout=PIPE, shell=True)
    pp = p.communicate()[0].decode("utf-8").strip()

    return pp # 문자열(str)


#
# 시간 기반 랜덤 함수
#
def trandom(length):
    from time import time
    """
    random 모듈이 좀 무겁다는 생각에...
    저녁 소여물 주면서 계속 랜덤함수를 생각했어요
    끊임없이 변하는것을 변하지 않는것으로 나누면 좋겠다싶었어요
    변하는건 시간, 변하지않는건 공간 그래서 이 함수가 만들어졌어요

    부처님오신날이라 그런지
     이게 꼭 불교의 윤회(자연의 순환-반복하는 이치)처럼 느껴졌습니다
    --소여물, 2020년 4월 30일 (음력 4월 8일)
    """
    _constant = 108 * 361 * 13 # 시간을 좀 더 세분화시킴
    vtime = int(time() * _constant)

    return vtime % length


#
# 파일 내려받기 함수
#
def _get_url(xyz):
    from subprocess import Popen, PIPE
    """
    `curl' 은 Linux/*BSD 등에서 유명한 도구입니다
    본 코드는 그래서 가급적 Linux/*BSD 시스템에서 실험하시길 권유드립니다
    """
    _cmd = "curl -s -f {0}".format(xyz)
    _try = Popen(_cmd, stdout=PIPE, shell=True)

    output = _try.communicate()[0].decode("utf-8").strip()

    return output # 문자열(str)


#
# hex ===> str
#
# 간단해보이지만 필요할때에 짜내려면
#  관련 문서를 찾아봐야할 정도로 가벼이 볼 수 없는 함수 
#
def hex_to_str(xyz):
    n = int(xyz, 16)

    return chr(n), xyz


#
# hbh ===> return string[::-1]
#
def hbh(string):
    return string[::-1]


# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2021년 10월 12일
