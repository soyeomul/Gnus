;;; ~/.gnus
;;; -*- coding: utf-8 -*-


(setq user-full-name "황병희") ;; 原始返本에 따라...
;; Card Holder: HWANG BYUNG HEE
;; Passport (여권): 황병희 / BYUNGHEE(FN) HWANG(LN)
;; RFC2822.From: 황병희 <soyeomul@gmail.com>
;; RFC2822.From: Byunghee HWANG <soyeomul@doraji.xyz>
;; RFC2822.From: Byunghee HWANG (황병희) <soyeomul@doraji.xyz>
(setq user-mail-address "soyeomul@gmail.com")
;; SMTP Email @gmail.com 이 기본값이며,
;; 뉴스그룹은 @doraji.xyz 를 기본값으로 사용함 
(setq message-from-style 'angles) ;; RFC2822


;;; ^Gmane_^))//
;; https://lars.ingebrigtsen.no/2020/01/15/news-gmane-org-is-now-news-gmane-io/
(setq gnus-select-method '(nntp "news.gmane.io"))


;;; This was the Usenet.
(add-to-list 'gnus-secondary-select-methods
	     '(nntp "september"
		    (nntp-address "news.eternal-september.org")))


;;; 다음
(add-to-list 'gnus-secondary-select-methods
	     '(nnimap "daum"
		      (nnimap-address "imap.daum.net")
		      (nnimap-server-port "imaps")
		      (nnimap-stream ssl)))


;;; Gmail
(add-to-list 'gnus-secondary-select-methods
	     '(nnimap "gmail"
		      (nnimap-address "imap.gmail.com")
		      (nnimap-server-port "imaps")
		      (nnimap-stream ssl)))


;;; GMX: 2023-05-26
(add-to-list 'gnus-secondary-select-methods
	     '(nnimap "gmx"
		      (nnimap-address "imap.gmx.com")
		      (nnimap-server-port "imaps")
		      (nnimap-stream ssl)))


(setq gnus-posting-styles
      '((".*"
	 ("X-Message-SMTP-Method" "smtp smtp.gmail.com 587")
	 (eval
	  (setq smtpmail-stream-type 'starttls))
	 (organization "金陵 (연원의 마음)"))
	("gmail:thanks"
	 ("X-Message-SMTP-Method" "smtp yw-1204.doraji.xyz 587")
	 (name "Byunghee HWANG")
	 (address "soyeomul@yw.doraji.xyz"))
	("gmane"
         ("X-Message-SMTP-Method" "smtp yw-1204.doraji.xyz 587")
	 (name "Byunghee HWANG")
	 (address "soyeomul@doraji.xyz"))
	((header "To" "soyeomul@doraji.xyz")
	 ("X-Message-SMTP-Method" "smtp yw-1204.doraji.xyz 587")
	 (name "Byunghee HWANG")
	 (address "soyeomul@doraji.xyz"))
	((header "Cc" "soyeomul@doraji.xyz")
	 ("X-Message-SMTP-Method" "smtp yw-1204.doraji.xyz 587")
	 (name "Byunghee HWANG")
	 (address "soyeomul@doraji.xyz"))
	("gmx"
	 ("X-Message-SMTP-Method" "smtp mail.gmx.com 587")
	 (address "byunghee.hwang@gmx.com"))
	("daum"
	 ("X-Message-SMTP-Method" "smtp smtp.daum.net 465")
	 (eval
	  (setq smtpmail-stream-type 'ssl))
	 (address "byunghee.hwang@daum.net"))))


;;; Color: gnus-header-from (기본값: "red3")
;; 사람 이름은 검정 글씨로 쓰는거라고 학교에서 배웠어유;;;
(require 'gnus-art)
(set-face-foreground 'gnus-header-from "black")


;;; 가입한 메일링 리스트 목록
(setq message-subscribed-addresses
      '("ding@gnus.org" ;; <soyeomul@doraji.xyz> / <byunghee.hwang@daum.net>
	"bind-users@lists.isc.org" ;; <soyeomul@doraji.xyz>
	"dane-users@list.sys4.de" ;; <soyeomul@doraji.xyz>
	"debian-bugs-dist@lists.debian.org" ;; <soyeomul@doraji.xyz>
	"debian-devel@lists.debian.org" ;; <soyeomul@doraji.xyz>
	"debian-devel-announce@lists.debian.org" ;; <soyeomul@doraji.xyz>
        "debian-emacsen@lists.debian.org" ;; <soyeomul@doraji.xyz>
	"debian-input-method@lists.debian.org" ;; <soyeomul@doraji.xyz>
	"debian-lists-test@lists.debian.org" ;; <soyeomul@doraji.xyz>
        "debian-python@lists.debian.org" ;; <soyeomul@doraji.xyz>
        "debian-user@lists.debian.org" ;; <soyeomul@doraji.xyz>
	"dmarc@ietf.org" ;; <soyeomul@doraji.xyz>
	"dns@list.cr.yp.to" ;; <soyeomul@doraji.xyz>
	"dnswl-users@dnswl.org" ;; <soyeomul@doraji.xyz>
	"emacs-devel@gnu.org" ;; <soyeomul@doraji.xyz>
	"emacs-humanities@gnu.org" ;; <soyeomul@doraji.xyz>
	"emacs-orgmode@gnu.org" ;; <soyeomul@doraji.xyz>
	"exim-users@lists.exim.org" ;; <soyeomul@doraji.xyz>
	"ezmlm@list.cr.yp.to" ;; <soyeomul@doraji.xyz>
	"evolution-users@lists.osuosl.org" ;; <soyeomul@doraji.xyz>
	"freebsd-test@freebsd.org" ;; <soyeomul@doraji.xyz>
	"git@vger.kernel.org" ;; <soyeomul@doraji.xyz>
        "gnupg-users@gnupg.org" ;; <soyeomul@doraji.xyz>
	"help-gnu-emacs@gnu.org" ;; <soyeomul@doraji.xyz>
	"ietf-dkim@ietf.org" ;; <soyeomul@doraji.xyz>
	"info-gnus-english@gnu.org" ;; <soyeomul@doraji.xyz>
	"lua-l@lists.lua.org" ;; <soyeomul@doraji.xyz>
	"mutt-users@mutt.org" ;; <soyeomul@doraji.xyz>
	"postfix-users@postfix.org" ;; <soyeomul@doraji.xyz>
	"postfix-users@de.postfix.org" ;; <soyeomul@doraji.xyz>
	"python-list@python.org" ;; <soyeomul@doraji.xyz>
	"qmail@list.cr.yp.to" ;; <soyeomul@doraji.xyz>
	"ubuntu-security-announce@lists.ubuntu.com" ;; <soyeomul@doraji.xyz>
	"ubuntu-users@lists.ubuntu.com" ;; <soyeomul@doraji.xyz>
        "users@spamassassin.apache.org" ;; <soyeomul@doraji.xyz> / <byunghee.hwang@daum.net>
       )
)


;;; Gnus Art Support
(setq gnus-treat-from-gravatar 'head)
(setq gnus-treat-mail-gravatar 'head)

;;; DEBUG
(setq smtpmail-debug-info t)
(setq smtpmail-debug-verb t)

;;; Gnus Version
(setq gnus-user-agent '(emacs gnus type))


;;; 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
;;; 마지막 갱신: 2024년 2월 23일

;;; 전자 메일 계정 현황
;; soyeomul@gmail.com: 한우업무 + 개인연락처 -- 그놈에볼루션 + Gnus
;; soyeomul@doraji.xyz: 리눅스/오픈소스 관련 인터넷 계정 -- Gnus
;; byunghee.hwang@gmx.com: 무한대의 저장을 위한 백업용 전자메일 계정 -- Gnus
;; byunghee.hwang@daum.net: 대한민국의 첫 전자메일 서비스
;; soyeomul@protonmail.ch (free account -- web)

;;; 참고문헌:
;; [0] https://en.wikipedia.org/wiki/Gnus
;; [1] http://quimby.gnus.org/
;; [2] https://ashish.blog/2017/09/gnus-multiple-smtp/
;; [3] https://workspaceupdates.googleblog.com/2020/03/less-secure-app-turn-off-suspended.html
;; [4] https://github.com/szepeviktor/debian-server-tools/blob/master/mail/README.md#transactional-email-providers
;; [5] https://www.mailop.org/best-practices/
;; [6] https://gitlab.com/soyeomul/stuff/-/blob/master/exim-mta-tutorial.pdf

;;; ^고맙습니다 _布德天下_ 감사합니다_^))//
