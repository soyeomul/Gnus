#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Path: /home/soyeomul/bin/getmail.py

# imap.gmail.com 에서 getmail 로
#  Inbox 와 gmane,
#  __gcp__,
#  __bullseye__
#  등으로 라벨링 된 폴더를 크롬북으로 동기화시키는 콤푸타 코드

import subprocess

PRE_CMD = "rm -f /tmp/fping.result"
CMD_FPING = "fping imap.gmail.com > /tmp/fping.result"
CMD_GETMAIL = "getmail \
--rcfile getmailrc-bullseye \
--rcfile getmailrc-inbox \
--rcfile getmailrc-gcp"

subprocess.call(PRE_CMD, shell=True)
subprocess.call(CMD_FPING, shell=True) 

f = open("/tmp/fping.result", "r")
result = f.read()

if "imap.gmail.com is alive" in str(result):
    subprocess.call(CMD_GETMAIL, shell=True)
else:
    exit(1)  
# 대략 fping 때려서 IMAP서버가 살아있으면 getmail 실행시킴.
# 만약, 핑반응 없으면 아무것도 하지 않음.

f.close()

# 우분투 18.04, 파이썬 3.6.7 에서 만들었습니다.

# 참고문헌: 
# getmail: http://pyropus.ca/software/getmail/

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2021년 3월 13일

'''
# getmail.py 를 크론탭에 등록함.
# 15분마다 동기화시킴.
MAILTO=soyeomul+cron@doraji.xyz
*/15 * * * * /home/soyeomul/bin/getmail.py
'''
