#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

sys.path.insert(0, "/home/soyeomul/__system__")
from lex__ import _full_path, trandom # custom module

import json

SRC_DIR = "~/gitlab/Gnus/faces"
DATA_DIR = _full_path(SRC_DIR)

def _output(_file):
    f = open("%s/%s" % (DATA_DIR, _file), "r")
    data = f.read(); f.close()

    dj = json.loads(data)
    length = len(dj.keys())
    idx = trandom(length)

    return dj['{0}'.format(idx)]

_file = sys.argv[1]

if __name__ == "__main__":
    print(_output(_file))

# 편집: GNU Emacs 27.1 (Ubuntu 18.04)
# 마지막 갱신: 2020년 9월 28일
