#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from subprocess import Popen, PIPE

def yw_era():
    cmd = "date +%'Y'"
    yeart = Popen(cmd, stdout=PIPE, shell=True)
    yeart_ = yeart.communicate()[0].decode("utf-8").strip()
    cpt = int(yeart_) - 1871 + 1

    return "%s %s%s" % (
        "대순",
        cpt,
        "년",
    )


thanks = "(%s) %s;" % (
    yw_era(),
    "천하창생을 끝까지 다 살리시려는 마음...^^^",
)


if __name__ == "__main__":
    print(thanks)

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2022년 3월 29일
