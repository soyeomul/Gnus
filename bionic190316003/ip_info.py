#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
import re
from datetime import datetime

# 쉘 명령어들의 조합으로 분석하고싶은 로그 파일 정렬
pre_cmd = "rm -f /tmp/log_src.txt"
cmd_log_src = "grep smtpd /var/log/mail.log | grep connect | grep from | \
grep -v disconnect | grep -v lost | grep -v localhost | \
tail -9 > /tmp/log_src.txt"
# ipinfo.io 서버에서 질의 많이 날리면 돈 내란 소리하므로, 소소하게 가장 마지막 접속 9줄만...
subprocess.call(pre_cmd, shell=True)
subprocess.call(cmd_log_src, shell=True)

# 정렬된 텍스트에서 IP 주소의 기반이 되는 문자열을 리스트로... 
f = open("/tmp/log_src.txt", "r")
lines = f.readlines()

result_0 = []
for x in lines:
    result_0.append(x.split()[0])
time_0 = result_0

result_1 = []
for x in lines:
    result_1.append(x.split()[1])
time_1 = result_1

result_2 = []
for x in lines:
    result_2.append(x.split()[2])
time_2 = result_2

result_7 = []
for x in lines:
    result_7.append(x.split()[7])
ip_src = result_7

# 메일서버 접속시간 문자열 조합...
def make_time(time_0, time_1, time_2):
    time = str(time_0)+"-"+str(time_1)+"-"+str(time_2)
    return time

# ip_src 를 기반으로 ip 와 ip_from 작업...
def make_ip(ip_src):
    exp = re.compile("\[(.*)]$")
    rs = exp.search(ip_src)
    rsg = rs.group(1)
    return rsg

def make_from(ip_src):
    ip = make_ip(ip_src)
    cmd_org = subprocess.Popen("curl -s ipinfo.io/{}/org".format(ip), stdout=subprocess.PIPE, shell=True)
    cmd_country = subprocess.Popen("curl -s ipinfo.io/{}/country".format(ip), stdout=subprocess.PIPE, shell=True)
    result_org = str(cmd_org.communicate()[0])[2:-3]
    result_country = str(cmd_country.communicate()[0])[2:-3]
    return result_country, result_org

# 람다 짱!!! 리스트 재작성~
time = list(map(lambda x, y, z: make_time(x, y, z), time_0, time_1, time_2))
ip = list(map(lambda xyz: "["+make_ip(xyz)+"]", ip_src))
ip_from = list(map(lambda xyz: make_from(xyz), ip_src))

# for 문으로 결과물 출력...
for v1, v2, v3 in zip(time, ip, ip_from):
    print(v1, v2, '\t', v3)

# 정보 갱신 시각
print("#Updated: %s UTC" % (str(datetime.utcnow())))

f.close()

# 편집: Emacs 26.1 (Ubuntu 18.04)
# 마지막 갱신: 2019년 3월 31일

# 참고문헌 (1): awk 의 유용함을 일깨워준 pchero님
# https://irclogs.ubuntu.com/2018/01/24/%23ubuntu-ko.html
# 참고문헌 (2): subprocess 개념을 잡아준 서니님
# https://irclogs.ubuntu.com/2019/01/25/%23ubuntu-ko.html
# 참고문헌 (3): 돈내라는소리만 아니하면 머찐 사이트!!! IP주소 관련 각종정보를 조회가능합니다~
# https://ipinfo.io/
