#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from datetime import datetime

today = datetime.utcnow().strftime("%Y-%m-%d")
vm_name = "bionic190316003"
cmd_ip_info = "/home/soyeomul/bin/ip_info.py"
mailto = "soyeomul+gcp@gmail.com"

subprocess.call("{0} | mail -s 'Connection records for {1} - {2}' {3}".format(cmd_ip_info, vm_name, today, mailto), shell=True)  

# __EOF__