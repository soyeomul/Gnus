#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# GnuPG 공개키 서버로 전송하기
# 참고문헌: <https://johngrib.github.io/wiki/gpg/>

import sys
import subprocess

KEY_SERVER = "keyserver.ubuntu.com"
KEY_ID = "68BB0234B99394CBB7E84F325752B4C89BF969A7"
cmd = "%s %s %s %s %s" % (
    'gpg',
    '--keyserver',
    KEY_SERVER,
    '--send-key',
    KEY_ID,
)

def 발사():
    try:
        subprocess.call(cmd, shell=True)
        print("ok!")
    except:
        sys.exit("some error... down...")
    
    return 0
    

if __name__ == "__main__":
    발사()

# 편집: GNU Emacs 27.1 (Debian 11 Bullseye)
# 마지막 갱신: 2021년 11월 6일
